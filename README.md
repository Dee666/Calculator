Kalkulator dengan Bahasa Python ( dan Indonesia :3 ), Coded By Dee666.

Requirement : Python 3.x
Platform : Windows / Linux / Android ( Termux )

Kalkulator untuk :
1. Bilangan Bulat dan Desimal
2. Pecahan
2. Luas Bangun Datar
3. Volume Bangun Ruang
4. Konversi Sistem Bilangan ( Decimal, Binary, Octal, Hexadecimal)
5. Rata - Rata